#!/usr/bin/env python
import os
import Tkinter, Tkconstants, tkFileDialog

class TkTreeRingAsk(Tkinter.Frame):

  def __init__(self, root):
    Tkinter.Frame.__init__(self, root)
    
    frame_opt = { 'expand': Tkconstants.TRUE, 'fill': Tkconstants.X }
    self.frmInput = Tkinter.Frame(self)
    self.frmOutput = Tkinter.Frame(self)
    self.frmExecute = Tkinter.Frame(self)
    self.frmInput.pack(**frame_opt)
    self.frmOutput.pack(**frame_opt)
    self.frmExecute.pack( expand=Tkconstants.TRUE, fill=Tkconstants.BOTH )

    # define filename variables
    self.filename_in = Tkinter.StringVar()
    self.filename_out = Tkinter.StringVar()
    
    # pack options
    button_opt = {'fill': Tkconstants.BOTH, 'padx': 5, 'pady': 5, 'side': Tkconstants.LEFT}
    entry_opt = {'expand': Tkconstants.TRUE, 'fill': Tkconstants.X, 'padx': 5, 'pady': 5, 'side': Tkconstants.LEFT}
    
    # define buttons and entry boxes
    self.label_opt = options = {}
    options['width'] = 25
    options['state'] = 'readonly'
    self.lblIn = Tkinter.Entry(self.frmInput, text='Input Filename', textvariable=self.filename_in, **self.label_opt)
    self.lblIn.pack(**entry_opt)
    Tkinter.Button(self.frmInput, text='Input', command=self.ask_inputfilename, width=8).pack(**button_opt)
    self.lblOut = Tkinter.Entry(self.frmOutput, text='Output Filename', textvariable=self.filename_out, **self.label_opt)
    self.lblOut.pack(**entry_opt)
    Tkinter.Button(self.frmOutput, text='Output', command=self.ask_outputfilename, width=8).pack(**button_opt)
    Tkinter.Button(self.frmExecute, text='Convert to Flat', command=self.convert2flat, width=25).pack(**button_opt)
    Tkinter.Button(self.frmExecute, text='Convert to Raw Decadal', command=self.convert2raw, width=25).pack(**button_opt)
    
    # define options for opening or saving a file
    self.file_opt = options = {}
    options['defaultextension'] = '' # couldn't figure out how this works
    options['filetypes'] = [('all files', '.*'), ('text files', '.txt'), ('raw files', '.rwl')]
    options['initialdir'] = os.getcwd()
    options['parent'] = root
    options['title'] = 'This is a title'

  def ask_inputfilename(self):
    # get filename
    options = self.file_opt.copy()
    options['title'] = 'Input Filename ...'
    self.filename_in.set( tkFileDialog.askopenfilename(**options) )
    self.lblIn.config(state=Tkconstants.NORMAL)
    self.lblIn.delete(0, Tkconstants.END)
    self.lblIn.insert(Tkconstants.END, self.filename_in.get())
    self.lblIn.config(state='readonly')
    self.lblIn.xview_moveto(1.0)

  def ask_outputfilename(self):
    # get filename
    options = self.file_opt.copy()
    options['title'] = 'Output Filename ...'
    self.filename_out.set( tkFileDialog.asksaveasfilename(**options) )
    self.lblOut.config(state=Tkconstants.NORMAL)
    self.lblOut.delete(0, Tkconstants.END)
    self.lblOut.insert(Tkconstants.END, self.filename_out.get())
    self.lblOut.config(state='readonly')
    self.lblOut.xview_moveto(1.0)

  def convert2flat(self):
    filename_in = self.filename_in.get()
    filename_out = self.filename_out.get()
    if filename_in and filename_out:
        print 'raw2flat( filename_in, filename_out )'
        print filename_in, filename_out
        self.quit()
    
  def convert2raw(self):
    filename_in = self.filename_in.get()
    filename_out = self.filename_out.get()
    if filename_in and filename_out:
        print 'flat2raw( filename_in, filename_out )'
        print filename_in, filename_out
        self.quit()
    
if __name__=='__main__':
  root = Tkinter.Tk()
  app = TkTreeRingAsk(root)
  app.pack(expand=Tkconstants.TRUE, fill=Tkconstants.BOTH)
  root.mainloop()

